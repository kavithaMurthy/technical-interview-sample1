package com.aftia.internal.technicalinterview;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import com.aftia.internal.technicalinterview.api.SampleAPI;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.Resource;

public class Main {

    private final static String BASE_RESOURCE_PATH = "static/index.html";

    public static void main(String[] args) throws Exception {
        Server server = new Server();
        ServerConnector connector = new ServerConnector(server);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);

        connector.setPort(8080);
        server.setConnectors(new Connector[] { connector });

        URI webRootUri = getBaseResource();
        
        context.setContextPath("/");
        context.setBaseResource(Resource.newResource(webRootUri));
        context.setWelcomeFiles(new String[] { "index.html" });

        ServletHolder servletHolder = new ServletHolder("default", DefaultServlet.class);
        servletHolder.setInitParameter("dirAllowed", "true");
        context.addServlet(servletHolder, "/");

        context.addServlet(SampleAPI.class.getName(), "/api");
        
        server.setHandler(context);

        server.start();
        server.join();
    }

    static String getBaseResourcePath(String fullPath) {
        return fullPath.substring(0, fullPath.lastIndexOf("/"));
    }

    static URI getBaseResource() throws URISyntaxException {
        ClassLoader cl = Main.class.getClassLoader();
        URL resource = cl.getResource(BASE_RESOURCE_PATH);
        if (resource == null) {
            throw new RuntimeException(String.format("Unable to find resource: %s", BASE_RESOURCE_PATH));
        }

        return new URI(getBaseResourcePath(resource.toURI().toString()));
    }
}
