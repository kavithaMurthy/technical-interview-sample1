package com.aftia.internal.technicalinterview.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class SampleAPI extends HttpServlet {

    private static final long serialVersionUID = -3182555380582310922L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String message = request.getParameter("message");
        
        response.setContentType("text/plain");
        response.setStatus(HttpServletResponse.SC_OK);

        response.getWriter().format("Hello! You said: %s", message);
    }

}
