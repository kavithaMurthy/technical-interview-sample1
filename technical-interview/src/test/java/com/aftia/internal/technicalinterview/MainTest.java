package com.aftia.internal.technicalinterview;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class MainTest {
    
    @Test
    public void testGetBaseResource(){

        String fullPath = "jar:file:/C:/Workspaces/Aftia/technical-interview-sample/technical-interview/target/technical-interview-1.0.0.jar!/static/index.html";

        String basePath = Main.getBaseResourcePath(fullPath);

        assertEquals("jar:file:/C:/Workspaces/Aftia/technical-interview-sample/technical-interview/target/technical-interview-1.0.0.jar!/static", 
            basePath,
            "Test base path");

    }
}
