# AFTIA Technical Interview

Welcome to the AFTIA Technical Interview public repository. This repository holds a project architecture that we use to test technical skills while working with tools that resemble our day-to-day tooling. As part of this exercise we will walk you through creating a copy of this repository and submitting your own changes.

## What should you prepare?

We expect candidates to use their own hardware (PC, laptop) to be able to install the required tools and to complete the technical evaluation below. 

### Technical Prerequisites
* Java 11 installed
* Java IDE of choice (VSCode, Eclipse, Intellij, etc.)
* Text Editor of choice (VSCode, Notepad++, Sublime, Atom, etc.)
* Modern (EverGreen) browser (Chrome, Firefox, etc.)
* A BitBucket Account
* GIT Client
* Maven 3.6+

### Logistics

* Location: Onsite or remote
* Expected Duration: 1-2h, at your own pace

## Getting Started
### Fork
In order to work on your technical test, you will first need to Fork a copy of this repository to your own account. This can be done by performing the following actions:

1. Click on the **+** button located on the left of the screen. Select **Fork this repository** and create a new copy within your personal account.

    ![](.assets/fork.png)

2. Clone this repository locally using the **Clone** button in the top left of the repository screen and your GIT client.

    ![](.assets/clone.png)

3. Using your locally cloned copy, create a [new feature branch](https://git-scm.com/docs/git-branch) from your own repository and [push](https://git-scm.com/docs/git-push) it to your personal copy of the repository.
    - Feature branches will start with the prefix `feature/` followed by the name of the feature you are working on.
4. Get to work! Using the information contained in the rest of this guide, start implementing the requested features. Commit as often as possible to your own feature branch and push the code back to the remote branch once done!

If you have any questions you can send an email to `techinterview@aftia.com` or to your interviewer. Additionaly, you can create a pull request and comment on the pull request ([see Creating a Pull Request](#creating-a-pull-request)) while tagging your reviewer in your comments.

## Testing

During this technical interview, we would like to test technical abilities relating to the following:

* Business Requirement Interpretation
* Customer Communication
* Java Development Experience
* HTML/JavaScript Development Experience
* Understanding of full-stack development

This technical interview will also test your interpretation and your ability to work with customers. In this context, your interviewer will play the role of a customer and you will play the role of a subject matter expert. Here are a few pointers:

* Treat the interviewer like you would a customer
* Ask as many questions as you need
* Use Google and the internet as much as you need

### Context

You are currently involved with a customer familiar with Agile methodologies. This customer has prepared a few user stories for you which they would like to see implemented. Go through these requirements with the customer and plan your next work iteration with them. Following that, implement the desired requirements. The user stories below are the effective requirements on which you will be working.

Some technical assumptions:
* You will be using the Java programming language for backend implementation
* You will be using HTML/CSS/JavaScript for frontend implementation
* All changes implemented below should be source controlled and pushed to your own repository.


| User Stories     |
| -----------------|
| As a backend developer, I want to retrieve JSON information from: https://api.publicapis.org/ where the *category* is equal to *vehicle* within a servlet, so that I can print all of the retrieved entries to the servlet response when invoked. |
| As a frontend developer, I want to create a webpage with a *teal* background, so that my webpage is “visually pleasing”. |
| As a frontend developer, I want a web page in my application to retrieve JSON information from a servlet in my application when a button is clicked, so that I can display the results to a user on the same webpage |
| As a frontend developer, I want all webpage assets (JS, CSS) to be loaded from external references, so that code readability is maintained. |
| As a backend developer, I want to use a public JSON library to parse JSON data, so that I can establish a proper data structure. |
| As a backend developer, I want new service methods to be unit tested (with at least 1 positive, and 1 negative unit test), so that I can ensure code quality is maintained |

### Sources

A Maven quickstart project example is available in the `technical-interview` folder. The sources contain the following:

* Backend Java resources: `src/main/java`
* Frontend HTML placeholder: `src/main/resources/static`

### How to Build

Make sure that you have [Maven](https://maven.apache.org/install.html) installed and your favorite Java IDE close by. 

To build the project:
- `mvn clean install`

To test the project:
- `mvn clean test`

To run the project *(this will allow live assets such as HTML, CSS, JavaScript to be reloaded in real-time when refreshing browsers)*:
- `mvn exec:java`

## Review
### Inviting a Reviewer

Perform the following steps to invite a reviewer to your repository:

1. Navigate to the **Repository settings** tab on the left of the screen

    ![](.assets/reposettings.png)

2. Select the **User and group access** tab, an add the reviewer to your repository by using their email address.

    ![](.assets/adduser.png)   
### Creating a Pull Request

Perform the following steps to create a new pull request:

1. Navigate to the **Pull requests** tab on the left of the screen and select **Create pull request**

    ![](.assets/createpr.png)

2. Fill out the information required to submit a new pull request.
    - Ensure that the pull request is configured to be merged into your own repository
    - Select the `master` branch when merging the pull request
    - Add your reviewer to the **Reviewers** field
    - Create the new pull request!

    ![](.assets/newpr.png)   
